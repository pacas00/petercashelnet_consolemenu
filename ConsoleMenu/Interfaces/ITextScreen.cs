﻿namespace PeterCashelNet.ConsoleMenu.Interfaces
{
    public interface ITextScreen
    {
        void setText(string s);
    }
}