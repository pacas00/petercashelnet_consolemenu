﻿using System;

namespace PeterCashelNet.ConsoleMenu.Interfaces
{
    public interface IMenuElement : IDisposable
    {
        void Call();
        string GetName();
    }

    public interface IMenuElement<T> : IDisposable
    {
        T Call();
        string GetName();
    }
}