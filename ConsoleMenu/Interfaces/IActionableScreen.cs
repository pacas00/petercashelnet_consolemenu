﻿namespace PeterCashelNet.ConsoleMenu.Interfaces
{
    public interface IActionableScreen : IScreen
    {
        IMenuElement GetOption(string key);
        bool HasOption(string key);
    }
}