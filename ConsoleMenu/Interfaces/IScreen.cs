﻿using PeterCashelNet.ConsoleMenu.MenuElements;
using System;

namespace PeterCashelNet.ConsoleMenu.Interfaces
{
    public interface IScreen : IDisposable
    {
        void Draw();
        string GetName();

        void ExecuteOnShow();
        bool ExecuteOnClose();
        void tick();

        MenuAction onTickAction
        {
            get;
            set;
        }

        MenuAction onShowAction
        {
            get;
            set;
        }

        MenuAction<bool> onCloseAction
        {
            get;
            set;
        }
    }
}