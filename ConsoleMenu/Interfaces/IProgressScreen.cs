﻿using PeterCashelNet.ConsoleMenu.MenuElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PeterCashelNet.ConsoleMenu.Interfaces
{
    public interface IProgressScreen : ITextScreen
    {
        void setProgress(int i);

        int getProgress();

        void setCompletedAction(MenuAction link);
        void callCompletedAction();

        void setCancelAction(MenuAction link);
    }
}
