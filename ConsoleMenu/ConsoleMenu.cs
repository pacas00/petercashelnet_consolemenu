﻿using PeterCashelNet.ConsoleMenu.Interfaces;
using PeterCashelNet.ConsoleMenu.MenuElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PeterCashelNet.ConsoleMenu
{
    public static class ConsoleMenu
    {
#if NET45
        public static bool isWindows = true;
        public static bool isLinux = false;
#endif
#if NETSTANDARD1_6
        public static bool isWindows = System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
        public static bool isLinux = System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Linux);
#endif

        public static bool cancelDraw = false;
        public static bool terminateMenu = false;
        public static bool suppressClear = true;
        public static bool displayOptionPrompt = false;
        private static bool needsRedraw = true;

#pragma warning disable CS0414
        private static IScreen CurrScreen = null;
#pragma warning restore CS0414
        public static IScreen rootScreen = null;

        public static int height = 0;
        public static int width = 0;

        public static char edgeChar = '\u2588';
        public static char backgroundChar = '\u2588';// '\u2593';
        public static char TLEdgeChar = '\u250C';
        public static char TREdgeChar = '\u2510';
        public static char BLEdgeChar = '\u2514';
        public static char BREdgeChar = '\u2518';
        public static char HEdgeChar = '\u2500';
        public static char VEdgeChar = '\u2502';

        public static string indent = "" + backgroundChar + backgroundChar + backgroundChar;

        public static ConsoleColor borderColour = Console.BackgroundColor;

        public static ConsoleColor backgroundColour
        {
            get
            {
                return borderColour;
            }
            set
            {
                borderColour = value;
            }
        }
        public static ConsoleColor textColour = Console.ForegroundColor;
        public static ConsoleColor menuColour = ConsoleColor.DarkBlue;

        public static ConsoleColor statusBarBackKeyColour = ConsoleColor.Gray;
        public static ConsoleColor statusBarKeyColour = ConsoleColor.Black;

        public static ConsoleColor statusBarBackColour = ConsoleColor.Blue;
        public static ConsoleColor statusBarTextColour = ConsoleColor.Cyan;


        /// <summary>
        /// Assign a void action to this for shutdown calls
        /// </summary>
        public static Action QuitHook = null;

        private static Dictionary<string, StatusBarOption> specialOpts = new Dictionary<string, StatusBarOption>();
        public static int tickIntervalMilli = 100;

        public static IScreen currScreen
        {
            get
            {
                return CurrScreen;
            }

            set
            {
                if (CurrScreen != null) if (!CurrScreen.ExecuteOnClose()) return; //For aborting the change
                CurrScreen = value;
                drawScreen();
                CurrScreen.ExecuteOnShow();
                needsRedraw = true;
            }
        }

        public static Thread MenuThread { get; private set; }

        public static void SetupConsoleMenu(IScreen root)
        {
            rootScreen = root;
            currScreen = root;

            height = Console.WindowHeight;
            width = Console.WindowWidth - 1;

            Console.OutputEncoding = Encoding.UTF8;

            if (isWindows) Console.BufferWidth = Console.WindowWidth;

            if (!specialOpts.ContainsKey("Q".ToUpper())) specialOpts.Add("Q".ToUpper(), new StatusBarOption("Quit", ConsoleMenu.Quit));

            resizeScreen();

            indent = "" + backgroundChar + backgroundChar + backgroundChar;
        }

        private static void resizeScreen()
        {

            height = Console.WindowHeight;
            width = Console.WindowWidth - 1;

            try //Try to set min size
            {
                if (Console.WindowHeight < 25 || Console.WindowWidth < 80)
                {
                    Console.WindowHeight = 25;
                    Console.WindowWidth = 80;
                    height = Console.WindowHeight;
                    width = Console.WindowWidth - 1;

                    if (isWindows) Console.BufferWidth = 25;
                }
                else
                {
                    if (isWindows) Console.BufferWidth = Console.WindowWidth;
                }
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
            { }
            Console.Clear();
            needsRedraw = true;

        }

        public static void SetNeedRedraw()
        {
            needsRedraw = true;
        }

        public static void AddSpecialOpt(string key, string name, Action func)
        {
            if (!specialOpts.ContainsKey(key.ToUpper())) specialOpts.Add(key.ToUpper(), new StatusBarOption(name, func));
        }

        public static void drawScreen(bool nestedCall = false)
        {
            //System.ArgumentOutOfRangeException

            try
            {
                if (!suppressClear) Console.Clear();
                else
                {
                    Console.CursorLeft = 0;
                    Console.CursorTop = 0;
                }
                setBorderColor();
                for (int i = 0; i < width; i++)
                {
                    Console.Write(edgeChar);
                }
                Console.WriteLine();
                WriteBanner();
                currScreen.Draw();
                WritePadding();
                WriteStatusBar();
                setTextColour();
                Console.CursorLeft = 6;
                Console.CursorTop = (height - 3);
                if (!(currScreen is ProgressScreen) && displayOptionPrompt) Console.Write("Please choose an option: ");
#pragma warning disable CS0168 // Variable is declared but never used
            }
            catch (System.ArgumentOutOfRangeException saoore)
#pragma warning restore CS0168 // Variable is declared but never used
            {
                if (!nestedCall)
                {
                    //Reset sizing
                    resizeScreen();
                    drawScreen(true);
                }
            }
            needsRedraw = false;
        }

        private static void WriteStatusBar()
        {
            setBorderColor();
            for (int i = 0; i < width; i++)
            {
                Console.Write(edgeChar);
            }
            Console.CursorLeft = 0;
            Console.CursorTop += 1;

            Console.ForegroundColor = statusBarBackColour;
            for (int i = 0; i < width; i++)
            {
                Console.Write(edgeChar);
            }

            Console.CursorLeft = 0;
            setTextColour();
            //Write into bar here

            foreach (string key in specialOpts.Keys)
            {
                Console.CursorLeft = Console.CursorLeft + 4;
                Console.BackgroundColor = statusBarBackKeyColour;
                Console.ForegroundColor = statusBarKeyColour;
                Console.Write(key);

                Console.BackgroundColor = statusBarBackColour;
                Console.ForegroundColor = statusBarTextColour;
                Console.Write(" " + specialOpts[key].GetName() + " ");
            }

            Console.CursorLeft = 0;
            Console.ForegroundColor = textColour;
            Console.BackgroundColor = backgroundColour;
        }

        private static void WritePadding()
        {
            setBorderColor();
            if (Console.CursorTop < (height - 2))
            {
                for (int i = Console.CursorTop; i < (height - 2); i++)
                {
                    Console.Write(edgeChar);
                    for (int j = Console.CursorLeft; j < width - 1; j++) Console.Write(backgroundChar);
                    Console.CursorLeft = width - 1;
                    Console.WriteLine(edgeChar);
                }
            }
            setTextColour();
        }

        private static void WriteBanner()
        {
            setBorderColor();
            Console.Write(edgeChar);
            for (int i = Console.CursorLeft; i < width - 1; i++) Console.Write(backgroundChar);
            Console.CursorLeft = width - 1;
            Console.WriteLine(edgeChar);


            Console.Write(edgeChar);
            for (int i = Console.CursorLeft; i < width - 1; i++) Console.Write(backgroundChar);

            int nameStartPos = ((width - currScreen.GetName().Length) / 2);
            int nameLength = currScreen.GetName().Length + 1;

            if ((currScreen.GetName().Length % 2) != 1) nameLength = nameLength + 1;
            int linePos = Console.CursorTop;
            {
                Console.CursorTop = linePos - 1;
                Console.CursorLeft = nameStartPos - 1;
                int endPos = Console.CursorLeft + nameLength;
                Console.BackgroundColor = menuColour;
                Console.Write(TLEdgeChar);
                for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(HEdgeChar);
                Console.Write(TREdgeChar);
                Console.BackgroundColor = backgroundColour;
                Console.CursorTop = linePos;
                Console.CursorLeft = 0;
            }

            Console.CursorLeft = nameStartPos - 1;
            Console.BackgroundColor = menuColour;
            Console.Write(VEdgeChar);
            setTextColour();
            Console.Write(currScreen.GetName());
            if (currScreen.GetName().Length < (nameLength - 1))
            {
                Console.Write(" ");
            }
            setBorderColor();
            Console.Write(VEdgeChar);
            Console.BackgroundColor = backgroundColour;
            Console.CursorLeft = width - 1;
            Console.WriteLine(edgeChar);
            setTextColour();

            {
                setBorderColor();
                Console.CursorTop = linePos + 1;
                Console.CursorLeft = 0;
                Console.Write(edgeChar);
                for (int i = Console.CursorLeft; i < width - 1; i++) Console.Write(backgroundChar);
                Console.CursorLeft = 1;
                Console.Write(indent);

                Console.CursorTop = linePos + 1;
                Console.CursorLeft = nameStartPos - 1;
                int endPos = Console.CursorLeft + nameLength;
                Console.BackgroundColor = menuColour;
                Console.Write(BLEdgeChar);
                for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(HEdgeChar);
                Console.Write(BREdgeChar);
                Console.BackgroundColor = backgroundColour;
                Console.CursorLeft = width - 1;
                Console.WriteLine(edgeChar);
                setTextColour();
            }

        }

        public static void setTextColour()
        {
            Console.ForegroundColor = textColour;
        }

        public static void setBorderColor()
        {
            Console.ForegroundColor = borderColour;
        }


        public static void Run()
        {
            //Setup Thread, Start it
            MenuThread = new Thread(new ThreadStart(threadRun));
            MenuThread.Start();
        }

        private static void threadRun()
        {
            while (!terminateMenu)
            {
                DateTime beginWait = DateTime.Now;
                if (!terminateMenu)
                {
                    DrawTick();
                    ProcessKey();
                    currScreen.tick();
                }
                if (terminateMenu) break;

                int milli = (int)DateTime.Now.Subtract(beginWait).TotalMilliseconds;
                if (milli < tickIntervalMilli) Thread.Sleep((tickIntervalMilli - milli));
            }
        }

        private static void ProcessKey()
        {
            string _char = string.Empty;

            if (Console.KeyAvailable)
            {
                needsRedraw = true;
                try
                {
                    var press = Console.ReadKey();
                    _char = press.KeyChar.ToString().ToUpper();
                    if (specialOpts.ContainsKey(_char))
                    {
                        specialOpts[_char].Call();
                        _char = string.Empty;
                    }
                    else
                    {
                        if (currScreen is IActionableScreen)
                        {
                            IActionableScreen scr = currScreen as IActionableScreen;
                            if ((scr.HasOption(_char)))
                            {
                                scr.GetOption(_char).Call();
                                _char = string.Empty;
                            }
                            else if (!(_char == string.Empty))
                            {
                                Console.CursorLeft = 4;
                                Console.Write(" .                              ");
                                Console.CursorLeft = 4;
                                Console.Write("Invalid Option: " + press.Key.ToString());
                                Thread.Sleep(1000);
                                needsRedraw = true;
                            }
                        }
                        else
                        {
                            if (!(_char == string.Empty))
                            {
                                Console.CursorLeft = 4;
                                Console.Write(" .                              ");
                                Console.CursorLeft = 4;
                                Console.Write("Invalid Option: " + press.Key.ToString());
                                Thread.Sleep(1000);
                                needsRedraw = true;
                            }
                        }
                    }
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                    //
                    if (!(_char == string.Empty))
                    {
                        Console.CursorLeft = 4;
                        Console.Write(" .                              ");
                        Console.CursorLeft = 4;
                        Console.Write("Invalid Option: " + _char);
                        Thread.Sleep(1000);
                    }

                }
            }
        }

        private static void DrawTick()
        {
            Console.ForegroundColor = textColour;
            Console.BackgroundColor = backgroundColour;
            Console.CursorVisible = false;
            if (!terminateMenu)
            {
                if (Console.BufferWidth < (Console.WindowWidth - 1) || Console.WindowHeight > height || (Console.WindowWidth - 1) > width) resizeScreen();
                if (needsRedraw) drawScreen();
            }
        }

        public static void Quit()
        {
            terminateMenu = true;
            if (QuitHook != null)
            {
                QuitHook.Invoke();
            }

        }
    }
}
