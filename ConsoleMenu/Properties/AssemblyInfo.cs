﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("PeterCashelNet.ConsoleMenu")]
[assembly: AssemblyDescription("Dos style menu system for C# Console Apps.")]
[assembly: AssemblyConfiguration("Release")]
[assembly: AssemblyCompany("Peter Cashel")]
[assembly: AssemblyProduct("PeterCashelNet.ConsoleMenu")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]



// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c50e1457-1d38-4356-a1ba-ddcb392397b4")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0")]
[assembly: AssemblyInformationalVersion("v1.1.0")]
[assembly: AssemblyFileVersion("1.1.0.0")]
