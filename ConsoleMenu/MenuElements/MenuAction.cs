﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PeterCashelNet.ConsoleMenu.Interfaces;
using PeterCashelNet.ConsoleMenu;

namespace PeterCashelNet.ConsoleMenu.MenuElements
{
    public class MenuAction : IDisposable, IMenuElement
    {
        string _Name;
#pragma warning disable CS0414
        Action _Delegate = null;
#pragma warning restore CS0414
        IScreen _Screen;

        public MenuAction(string name, IScreen screen, Action function = null) 
        {
            _Name = name;
            _Screen = screen;
            if (function != null) _Delegate = function;
        }

        public MenuAction(string name, Action function, IScreen screen = null) 
        {
            _Name = name;
            _Delegate = function; 
            if (screen != null) _Screen = screen;
        }

        public void Dispose()
        {
            if (_Screen != null) _Screen.Dispose();
            _Name = null;
            _Screen = null;
            _Delegate = null;
        }

        
        public void Call()
        {
            if (_Delegate != null) _Delegate.Invoke();
            if (_Screen != null) ConsoleMenu.currScreen = _Screen;
        }

        public string GetName()
        {
            return _Name;
        }
    }
}

public class MenuAction<T> : IDisposable, IMenuElement<T>
{
    string _Name;
#pragma warning disable CS0414
    Func<T> _Delegate = null;
#pragma warning restore CS0414
    IScreen _Screen;

    public MenuAction(string name, IScreen screen, Func<T> function = null)
    {
        _Name = name;
        _Screen = screen;
        if (function != null) _Delegate = function;
    }

    public MenuAction(string name, Func<T> function, IScreen screen = null)
    {
        _Name = name;
        _Delegate = function;
        if (screen != null) _Screen = screen;
    }

    public void Dispose()
    {
        if (_Screen != null) _Screen.Dispose();
        _Name = null;
        _Screen = null;
        _Delegate = null;
    }


    public T Call()
    {
        T result = default(T);
        if (_Delegate != null) _Delegate.Invoke();
        if (_Screen != null) ConsoleMenu.currScreen = _Screen;
        return result;
    }

    public string GetName()
    {
        return _Name;
    }
}