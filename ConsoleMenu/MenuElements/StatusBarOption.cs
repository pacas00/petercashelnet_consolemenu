﻿using PeterCashelNet.ConsoleMenu.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeterCashelNet.ConsoleMenu.MenuElements
{
    public class StatusBarOption : IMenuElement
    {
        string _Name;
        Action _Delegate;

        public StatusBarOption(string name, Action function)
        {
            _Name = name;
            _Delegate = function;
        }

        public void Dispose()
        {
            _Name = null;
            _Delegate = null;
        }

        public void Call()
        {
            _Delegate.Invoke();
        }

        public string GetName()
        {
            return _Name;
        }        
    }
}
