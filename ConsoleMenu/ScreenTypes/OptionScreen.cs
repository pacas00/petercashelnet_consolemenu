﻿using PeterCashelNet.ConsoleMenu.Interfaces;
using PeterCashelNet.ConsoleMenu.MenuElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PeterCashelNet.ConsoleMenu.ConsoleMenu;

namespace PeterCashelNet.ConsoleMenu
{
    public class OptionScreen : IScreen, IActionableScreen
    {
        public Dictionary<string, IMenuElement> items = new Dictionary<string, IMenuElement>();

        public string ScreenName = "";
        private bool centeredText = false;

        
        private MenuAction showAction = null;
        private MenuAction<bool> closeAction = null;
        private MenuAction tickAction = null;

        public MenuAction onTickAction
        {
            get
            {
                return tickAction;
            }

            set
            {
                tickAction = value;
            }
        }

        public MenuAction onShowAction
        {
            get
            {
                return showAction;
            }

            set
            {
                showAction = value;
            }
        }

        public MenuAction<bool> onCloseAction
        {
            get
            {
                return closeAction;
            }

            set
            {
                closeAction = value;
            }
        }

        public OptionScreen()
        {
        }

        public OptionScreen(string name)
        {
            ScreenName = name;
        }

        public bool AddItem(string key, IMenuElement item)
        {
            if (items.ContainsKey(key))
            {
                return false;
            } else
            {
                items.Add(key, item);
                return items.ContainsKey(key);
            }
        }

        public bool HasOption(string key)
        {
            return items.ContainsKey(key);
        }

        public IMenuElement GetOption(string key)
        {
            return items[key];
        }

        public void Dispose()
        {
            foreach (MenuAction item in items.Values)
            {
                item.Dispose();
            }
            ScreenName = null;
        }

        public void Draw()
        {
            //Try to centre items
            int line = Console.CursorTop + 2;
            int pos = ((height - 1) - 12) / 2;

            if (pos < line) pos = line;
            int length = 0;

            foreach (string key in items.Keys)
            {
                if (items[key].GetName().Length > length) length = items[key].GetName().Length;
                if ((length) > (width - 16)) length = width - 16;
            }

            length = width - 16;

            length = length + indent.Length + 4; //longest string length, length of indent, 2 for border plus start and end space.

            setBorderColor();
            for (int i = Console.CursorTop; i < pos; i++)
            {
                Console.Write(edgeChar);
                for (int j = Console.CursorLeft; j < width - 1; j++) Console.Write(backgroundChar);
                Console.CursorLeft = width - 1;
                Console.WriteLine(edgeChar);
            }

            Console.CursorTop = pos - 1;

            Console.Write(edgeChar);
            int endPos = Console.CursorLeft + length + 3;
            Console.Write(indent);
            Console.BackgroundColor = menuColour;
            Console.Write(TLEdgeChar);
            for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(HEdgeChar);
            Console.Write(TREdgeChar);
            Console.BackgroundColor = backgroundColour;
            Console.CursorTop = pos;
            Console.CursorLeft = 0;

            //Write items
            foreach (string key in items.Keys)
            {
                Console.Write(edgeChar);
                for (int i = Console.CursorLeft; i < width - 1; i++) Console.Write(backgroundChar);
                Console.CursorLeft = 1;
                Console.Write(indent);
                endPos = Console.CursorLeft + length;
                Console.BackgroundColor = menuColour;
                Console.Write(VEdgeChar);
                setTextColour();

                if (centeredText)
                {
                    Console.ForegroundColor = menuColour;
                    //Offset cursor
                    int len = (" " + key + " - " + items[key].GetName()).Length / 2;
                    int offset = ((length / 2) - (len)) - 1;
                    for (int off = 0; off < offset; off++) Console.Write(edgeChar);
                    setTextColour();
                }

                Console.Write(" " + key + " - " + items[key].GetName());
                for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(" ");
                setBorderColor();
                Console.Write(VEdgeChar);
                Console.BackgroundColor = backgroundColour;
                Console.CursorLeft = width - 1;
                Console.WriteLine(edgeChar);
            }

            Console.Write(edgeChar);
            endPos = Console.CursorLeft + length + 3;
            Console.Write(indent);
            Console.BackgroundColor = menuColour;
            Console.Write(BLEdgeChar);
            for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(HEdgeChar);
            Console.Write(BREdgeChar);
            Console.BackgroundColor = backgroundColour;
            for (int j = Console.CursorLeft; j < width - 1; j++) Console.Write(backgroundChar);
            Console.CursorLeft = width - 1;
            Console.WriteLine(edgeChar);

            setTextColour();
        }

        public string GetName()
        {
            return ScreenName;
        }

        //public void setCenteredText(bool value)
        //{
        //    centeredText = value;
        //}

        //public bool getCenteredText()
        //{
        //    return centeredText;
        //}

        public bool ExecuteOnClose() //if you need to abort screen change, use onCloseAction, return false to abort
        {
            if (onCloseAction != null) return onCloseAction.Call();
            else return true;
        }
        public void ExecuteOnShow() //if this is wanted, extend and override
        {
            if (onShowAction != null) onShowAction.Call();
        }
        public void tick() //If your screen needs to tick, this.
        {
            if (onTickAction != null) onTickAction.Call();
        }
    }
}
