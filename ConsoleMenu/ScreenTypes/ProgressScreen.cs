﻿using PeterCashelNet.ConsoleMenu.Interfaces;
using PeterCashelNet.ConsoleMenu.MenuElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PeterCashelNet.ConsoleMenu.ConsoleMenu;

namespace PeterCashelNet.ConsoleMenu
{
    public class ProgressScreen : IScreen, IActionableScreen, IProgressScreen
    {
        public string ScreenName = "";
        public List<string> textLines = new List<string>();
        private int progress = 0;
        private MenuAction mScreenLink = null;
        private MenuAction cancelAction = null;

        private MenuAction showAction = null;
        private MenuAction<bool> closeAction = null;
        private MenuAction tickAction = null;

        public MenuAction onTickAction
        {
            get
            {
                return tickAction;
            }

            set
            {
                tickAction = value;
            }
        }

        public MenuAction onShowAction
        {
            get
            {
                return showAction;
            }

            set
            {
                showAction = value;
            }
        }

        public MenuAction<bool> onCloseAction
        {
            get
            {
                return closeAction;
            }

            set
            {
                closeAction = value;
            }
        }

        public ProgressScreen()
        {
        }

        public ProgressScreen(string name)
        {
            ScreenName = name;
        }

        public void Dispose()
        {
            ScreenName = null;
        }

        public void Draw()
        {
            //Try to centre items
            int line = Console.CursorTop + 2;
            int pos = ((height - 1) - 12) / 2;

            if (pos < line) pos = line;
            int length = 0;

            foreach (string key in textLines)
            {
                if (key.Length > length) length = key.Length;
                if ((length ) > (width - 16)) length = width - 16;
            }

            //Hard set this
            length = width - 16;

            length = length + indent.Length + 4; //longest string length, length of indent, 2 for border plus start and end space.

            setBorderColor();
            for (int i = Console.CursorTop; i < pos; i++)
            {
                Console.Write(edgeChar);
                for (int j = Console.CursorLeft; j < width - 1; j++) Console.Write(backgroundChar);
                Console.CursorLeft = width - 1;
                Console.WriteLine(edgeChar);
            }

            Console.CursorTop = pos - 1;

            Console.Write(edgeChar);
            int endPos = Console.CursorLeft + length + 3;
            Console.Write(indent);
            Console.BackgroundColor = menuColour;
            Console.Write(TLEdgeChar);
            for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(HEdgeChar);
            Console.Write(TREdgeChar);
            Console.BackgroundColor = backgroundColour;
            Console.CursorTop = pos;
            Console.CursorLeft = 0;

            //Write items
            foreach (string key in textLines)
            {
                Console.Write(edgeChar);
                for (int i = Console.CursorLeft; i < width - 1; i++) Console.Write(backgroundChar);
                Console.CursorLeft = 1;
                Console.Write(indent);
                endPos = Console.CursorLeft + length;
                Console.BackgroundColor = menuColour;
                Console.Write(VEdgeChar);
                setTextColour();
                for (int i = 0; i < length; i++)
                {
                    if (i < key.Length && (i < length - 1)) Console.Write(key[i]);
                }

                for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(" ");
                setBorderColor();
                Console.Write(VEdgeChar);
                Console.BackgroundColor = backgroundColour;
                Console.CursorLeft = width - 1;
                Console.WriteLine(edgeChar);
            }
            //Blank over line
            {
                Console.Write(edgeChar);
                for (int i = Console.CursorLeft; i < width - 1; i++) Console.Write(backgroundChar);
                Console.CursorLeft = 1;
                Console.Write(indent);
                endPos = Console.CursorLeft + length;
                Console.BackgroundColor = menuColour;
                Console.Write(VEdgeChar);
                setTextColour();

                for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(" ");
                setBorderColor();
                Console.Write(VEdgeChar);
                Console.BackgroundColor = backgroundColour;
                Console.CursorLeft = width - 1;
                Console.WriteLine(edgeChar);
            }

            //progress
            {
                Console.Write(edgeChar);
                for (int i = Console.CursorLeft; i < width - 1; i++) Console.Write(backgroundChar);
                Console.CursorLeft = 1;
                Console.Write(indent);
                endPos = Console.CursorLeft + length;
                Console.BackgroundColor = menuColour;
                Console.Write(VEdgeChar);
                setTextColour();

                int prog = (int)  (((double)progress / 100.0d) * (double)((double)length - 1.0d) );

                for (int i = 0; i < prog; i++)
                {
                    Console.Write('\u2588');
                }

                for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(" ");
                setBorderColor();
                Console.Write(VEdgeChar);
                Console.BackgroundColor = backgroundColour;
                Console.CursorLeft = width - 1;
                Console.WriteLine(edgeChar);
            }


            //Blank under line
            {
                Console.Write(edgeChar);
                for (int i = Console.CursorLeft; i < width - 1; i++) Console.Write(backgroundChar);
                Console.CursorLeft = 1;
                Console.Write(indent);
                endPos = Console.CursorLeft + length;
                Console.BackgroundColor = menuColour;
                Console.Write(VEdgeChar);
                setTextColour();

                for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(" ");
                setBorderColor();
                Console.Write(VEdgeChar);
                Console.BackgroundColor = backgroundColour;
                Console.CursorLeft = width - 1;
                Console.WriteLine(edgeChar);
            }

            Console.Write(edgeChar);
            endPos = Console.CursorLeft + length + 3;
            Console.Write(indent);
            Console.BackgroundColor = menuColour;
            Console.Write(BLEdgeChar);
            for (int i = Console.CursorLeft; i < (endPos); i++) Console.Write(HEdgeChar);
            Console.Write(BREdgeChar);
            Console.BackgroundColor = backgroundColour;
            for (int j = Console.CursorLeft; j < width - 1; j++) Console.Write(backgroundChar);
            Console.CursorLeft = width - 1;
            Console.WriteLine(edgeChar);

            setTextColour();
        }

        public string GetName()
        {
            return ScreenName;
        }

        public IMenuElement GetOption(string key)
        {
            return cancelAction;
        }

        public bool HasOption(string key)
        {
            if (cancelAction != null && key == "\u001b") return true;
            return false;
        }

        public void setProgress(int i)
        {
            progress = i;
            ConsoleMenu.SetNeedRedraw();
        }

        public int getProgress()
        {
            return progress;
        }

        public void setText(string s)
        {
            textLines = s.Split('\n').ToList();
            ConsoleMenu.SetNeedRedraw();
        }

        public void setCompletedAction(MenuAction link)
        {
            mScreenLink = link;
        }

        public void callCompletedAction()
        {
            if (mScreenLink != null) mScreenLink.Call();
        }

        public void setCancelAction(MenuAction link)
        {
            cancelAction = link;
        }


        public bool ExecuteOnClose() //if you need to abort screen change, use onCloseAction, return false to abort
        {
            if (onCloseAction != null) return onCloseAction.Call();
            else return true;
        }
        public void ExecuteOnShow() //if this is wanted, extend and override
        {
            if (onShowAction != null) onShowAction.Call();
        }
        public void tick() //If your screen needs to tick, this.
        {
            if (onTickAction != null) onTickAction.Call();
        }
    }
}
