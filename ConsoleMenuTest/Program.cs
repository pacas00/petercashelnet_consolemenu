﻿using PeterCashelNet.ConsoleMenu;
using PeterCashelNet.ConsoleMenu.MenuElements;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetCoreConsoleTest
{
    public class Program
    {
        static OptionScreen root = new OptionScreen("Main Menu");
        static OptionScreen screen2 = new OptionScreen("OptionScreen 2");
        static ProgressScreen screen4 = new ProgressScreen("ProgressScreen 4");
        static TextScreen screen3 = new TextScreen("TextScreen 3", root);
        private static Timer t;

        public static void Main(string[] args)
        {
            
            screen4.setText("Example Progress Screen\nWith Lines of Text\nNeat Huh?");
            screen4.setCompletedAction(new MenuAction("Back to Main Menu", root));

            screen3.textLines.Add("Hello");
            screen3.textLines.Add("Line two");
            screen3.textLines.Add("ARABLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBLBL");
            screen3.textLines.Add("We dont text wrap, not my problem,");
            screen3.textLines.Add("....");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("");
            screen3.textLines.Add("Press enter to go to next screen");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.textLines.Add("Welp");
            screen3.setReducedPadding();

            MenuAction rootitem1 = new MenuAction("Open OptionScreen", screen2);
            MenuAction rootitem2 = new MenuAction("Print Hello World", HelloWorld);
            MenuAction rootitem3 = new MenuAction("Quit", ConsoleMenu.Quit);
            MenuAction rootitem4 = new MenuAction("Open TextScreen", screen3);
            MenuAction rootitem5 = new MenuAction("Open ProgressScreen, with timer action to test progress", screen4, progressTestTimer);
            MenuAction rootitem6 = new MenuAction("Toggle centered text on textscreen", txtCenter);

            MenuAction screen2item1 = new MenuAction("Back to Main Menu", root);
            MenuAction screen2item2 = new MenuAction("Quit", ConsoleMenu.Quit);

            root.AddItem("1", rootitem1);
            root.AddItem("2", rootitem2);
            root.AddItem("3", rootitem3);
            root.AddItem("4", rootitem4);
            root.AddItem("5", rootitem5);
            root.AddItem("T", rootitem6);

            screen2.AddItem("1", screen2item1);
            screen2.AddItem("2", screen2item2);

            ConsoleMenu.SetupConsoleMenu(root);

            ConsoleMenu.menuColour = ConsoleColor.DarkBlue;
            ConsoleMenu.borderColour = ConsoleColor.DarkGray; 
            ConsoleMenu.textColour = ConsoleColor.White;

            ConsoleMenu.Run();
        }

        private static void txtCenter()
        {
            screen3.setCenteredText(!screen3.getCenteredText());
            ConsoleMenu.SetNeedRedraw();
        }

        private static void progressTestTimer()
        {
            //if (progress >= 101) new MenuScreenAction("Back to Main Menu", ConsoleMenu.rootScreen).Call();
            //else progress += 5;
            screen4.setProgress(0);
            var autoEvent = new AutoResetEvent(true);
            t = new Timer(T_Elapsed, autoEvent, 250, 505);
            
        }

        private static void T_Elapsed(Object stateInfo)
        {
            int progress = screen4.getProgress();

            if (progress < 100) screen4.setProgress(progress + 5);
            else
            {
                screen4.callCompletedAction();
                //AutoResetEvent autoEvent = (AutoResetEvent)stateInfo;
                //autoEvent.Set();
                t.Dispose();
            }

        }

        private static void HelloWorld()
        {
            Console.Clear();
            Console.WriteLine("Hello World");
            Thread.Sleep(250);
            ConsoleMenu.SetNeedRedraw();
        }
    }
}
