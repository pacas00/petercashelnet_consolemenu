# README #

This repository is the home of PeterCashelNet.ConsoleMenu


What is this?
=============
This project is a simple DOS style menu for use in C# Console Apps.


Why?
====
Nostalgia.


Example on how to use
========================
Install the package via nuget

```
#!c#

PM> Install-Package PeterCashelNet.ConsoleMenu
```

Then create a root screen (main menu) and a second screen
```
#!c#
ConsoleMenu_Screen root = new ConsoleMenu_Screen("Main Menu");
ConsoleMenu_Screen screen2 = new ConsoleMenu_Screen("Screen 2");
```

Then create some menu items to put on the screen. There are 2:

* ConsoleMenu_ItemScreenLink(string name, ConsoleMenu_Screen screen)   - Switches to a different screen
* ConsoleMenu_Item(string name, action method)                         - Runs a method with no method arguments and a void return type.

```
#!c#
                                                            
ConsoleMenu_Item rootitem1 = new ConsoleMenu_ItemScreenLink("Open Screen 2", screen2);
ConsoleMenu_Item rootitem2 = new ConsoleMenu_Item("Hello World", HelloWorld);
ConsoleMenu_Item rootitem3 = new ConsoleMenu_Item("Quit", ConsoleMenu.Quit);

ConsoleMenu_Item screen2item1 = new ConsoleMenu_ItemScreenLink("Back to Main Menu", root);
ConsoleMenu_Item screen2item2 = new ConsoleMenu_Item("Quit", ConsoleMenu.Quit);
```

Once created, add them to the screens with the screen's method addItem(string key, ConsoleMenu_Item item) - Works for both types of item.

```
#!c#
root.addItem("1", rootitem1);
root.addItem("H", rootitem2);
root.addItem("3", rootitem3);

screen2.addItem("1", screen2item1);
screen2.addItem("2", screen2item2);
```

Run ConsoleMenu's SetupConsoleMenu method, with the main menu screen as the parameter, to get the ConsoleMenu ready to go.
```
#!c#
ConsoleMenu.SetupConsoleMenu(root);
```

Optionally set different colours for the menu.
```
#!c#
ConsoleMenu.backgroundColour = ConsoleColor.Black;
ConsoleMenu.borderColour = ConsoleColor.DarkGray;
ConsoleMenu.textColour = ConsoleColor.White;
```

And finally run the menu.
```
#!c#
ConsoleMenu.Run();
```

Check out this example at https://bitbucket.org/pacas00/petercashelnet_consolemenu/